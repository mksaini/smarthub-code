/*
 * Cloud.h
 *
 *  Created on: 2013-10-11
 *      Author: mukesh
 */

#ifndef CLOUD_H_
#define CLOUD_H_

#define ERROR    -1
#define MAX_CLIENTS    10
#define MAX_DATA    1024

struct threadParams {
	void * instance;
	int socketID;
	int threadID;
};


#endif /* CLOUD_H_ */
