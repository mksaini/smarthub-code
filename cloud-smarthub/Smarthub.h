/*
 * Smarthub.h
 *
 *  Created on: 2013-10-11
 *      Author: mukesh
 */



#ifndef SMARTHUB_H_
#define SMARTHUB_H_


using namespace std;

#define RECENT_HISTORY 1000// equivalent to almost 40 seconds
#define SEGMENT_SIZE 100 // Number of frames in each segment


#include"Video.h"

class SmartHub {

private:
	//To read from from files or IP addresses
	vector<CvCapture*> capture;

	//The video is read in these frames
	vector<IplImage*> frame;

	//Video objects for each video, future smarthub may have audio also
	vector<Video*> video;



	//Total number of cameras should be known to all functions
	int NUM_OF_CAMERAS;



public:

	//If there is any error occurs, kill the thread and indicate its dead by setting alive = -1
	int alive;

	//Flags to indicate required processing steps
	bool process;



	//Structure to store history values
	struct _snapHist { //later this will be stored in the database
		int people;//number of people

	} snapHist[RECENT_HISTORY];//stores client preferences

	//vector to store video names or IP addresses
	//vector<string> camAddrVector;


public:
	//constructor1
	SmartHub();

	//constructor2:
	SmartHub(int num_of_cameras);

	~SmartHub();

	//Write initialization here
	int InitializeVars(vector<string> camAddrVector);

	//This is the main thread that runs in an infinite loop
	void MainThread();

	void KillMainThread();

};



#endif /* SMARTHUB_H_ */
