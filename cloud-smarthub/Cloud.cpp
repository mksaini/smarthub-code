/*
 * Cloud.h
 *
 *  Created on: 2013-10-11
 *      Author: mukesh
 */
#include<cv.h> /* required to use OpenCV */
#include<highgui.h> /* required to use OpenCV's highgui */
#include<stdio.h>
#include <sys/time.h>
#include "sys/socket.h"
#include "sys/types.h"
#include "netinet/in.h"
#include "error.h"
#include "strings.h"
#include "unistd.h"
#include "arpa/inet.h"

#include"Cloud.h"
#include"Smarthub.h"
//#include"Capture.h"
//#include"Frame.h"



pthread_t thpool[MAX_CLIENTS];//thread ids are stored here
pthread_attr_t thpool_attr[MAX_CLIENTS];
int thread_table[MAX_CLIENTS];

using namespace std;
using namespace cv;

void* start_thread(void* arg) {
	struct threadParams *thx = (struct threadParams *) arg;

	//printf("In start_thread thx->val = %d", thx->val);
	static_cast<SmartHub*>(thx->instance)->MainThread();
	cout<<"\n Thread finished";
}


void* control_thread(void* arg)
{
	struct threadParams* th= (struct threadParams*)arg;
	int new_socket = th->socketID;
	int threadID = th->threadID;
	int data_len = 1;
	vector<string> camAddrVector;
	char data[MAX_DATA];
	int num_of_cameras=0;

	//Start a data thread with SmartHub
	pthread_t smart_thread;
	pthread_attr_t smart_thread_attr;


//===INITIALIZATIONS===//

    //Get the initial message from the client
	 data_len = recv(new_socket, data, MAX_DATA, 0);
	 data[data_len] = '\0';
	 printf("Recieved mesg from client: %s\n", data);

	 // Convert message to ofstream to easily extract video names
	 stringstream client_message(data);
	 string cam_addr;

	 //Assume the client message contains video addresses separated by spaces
	 while(1){

		 cam_addr=" ";
		 client_message >> cam_addr;
		 //To identify that we have scanned all names
		 if (cam_addr==" ")
			break;

		 else
		    camAddrVector.push_back(cam_addr);
    }

	//Obtain total number of cameras
	num_of_cameras = camAddrVector.size();

	//Instantiate Smarthub object to process video from cameras
	SmartHub* smarthub = new SmartHub(num_of_cameras);

	//Initialize the object variables
	smarthub->InitializeVars(camAddrVector);
	if(smarthub->alive) //If there is any problem with initializing the variables, kill the thread
	{
		string tcp_message="Initialization Successful!";
		send(new_socket, tcp_message.c_str(), tcp_message.length(), 0);
		//Summarize initiaalization part
		cout<<"Creating a smarthub instance to process "<<num_of_cameras<<" video cameras\n";

	}
	else
	{
		string tcp_message="Initialization Error. Closing Socket";
		send(new_socket, tcp_message.c_str(), tcp_message.length(), 0);
	}


	//===MAIN QUERY-RESPONSE LOOP===//
	 data_len = 1;
     while(data_len && smarthub->alive)
     {

    	 data_len = recv(new_socket, data, MAX_DATA, 0);
         //Processing step

    	 if (strncmp(data, "bye", 3) == 0){
    		 cout<<"Deleting the smarthub instance";
    		 smarthub->KillMainThread();

    		 string tcp_message="bye bye";
 			 send(new_socket, tcp_message.c_str(), tcp_message.length(), 0);

 			 break;
    	 }
    	 if (strncmp(data, "process", 3) == 0){
    		 if (!smarthub->process)
    		 {
    		        struct threadParams arg;
    		        arg.instance = smarthub;
    		        arg.threadID = 0;
    		        arg.socketID = 0;
    		        //Initialize the attributes
    		        pthread_attr_init(&smart_thread_attr);
    		        pthread_create(&smart_thread, &smart_thread_attr, start_thread, &arg);

    				string tcp_message="Now processing";
    				send(new_socket, tcp_message.c_str(), tcp_message.length(), 0);
    		 }

    	 }
    	 else
    	 {
			 for( int i = 0; data[ i ]; i++)
			 {
				 if(data[i]=='a' || data[i]=='e' || data[i]=='i' ||data[i]=='o' || data[i]=='u' )
					 data[ i ] = toupper( data[ i ] );
				 else
				 data[ i ] = data[ i ];

			 }
			 ////////

			 if(data_len)
			 {

				 send(new_socket, data, data_len, 0);
				 data[data_len] = '\0';
				 printf("\nSent mesg to client: %s", data);
			 }
    	 }

     }


     pthread_join(smart_thread, NULL);
     printf("\nClient disconnected\n");
     thread_table[threadID]=0;
     close(new_socket);
   	 //delete smarthub;
	 pthread_exit(NULL);
}

int look4FreeThreadID()
{
    //make all sockets available
    for (int i=0; i<MAX_CLIENTS; i++)
    	if(!thread_table[i])
    		return i;
    //if no slots are available
    return -1;
}


// A Simple Camera Capture Framework
int main(int argc, char **argv)
{

	//===============Parse the command line arguments here=======================
	if(argc!=2){
		fprintf( stderr, "Usage: ./cloud port_number\n");
		return -1;
	}

	//TCP Socket variables
	struct sockaddr_in server;
    struct sockaddr_in client;
    int sockaddr_len = sizeof(struct sockaddr_in);
    int data_len;
    char data[MAX_DATA]; //To hold the data packet to send over socket.
    int sock;
    int new_socket;

    //make all sockets available
    for (int i=0; i<MAX_CLIENTS; i++)
    	thread_table[i]=0;

    //Open the cloud server socket (TCP type)
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) == ERROR)
    {
        perror("server socket: ");
        exit(-1);
    }

    //Initialize server attributes
    server.sin_family = AF_INET;
    server.sin_port = htons(atoi(argv[1]));
    server.sin_addr.s_addr = INADDR_ANY;
    bzero(&server.sin_zero, 8);

    //Bind the server to the port number supplied as input to the program
    if((bind(sock, (struct sockaddr *)&server, sockaddr_len)) == ERROR)
    {
        perror("bind : ");
        exit(-1);
    }

    //This sets the server in the listening mode forever
    if((listen(sock, MAX_CLIENTS)) == ERROR)
    {
        perror("listen");
        exit(-1);
    }
    printf("\nThe TCPServer Waiting for client on port %d\n",ntohs(server.sin_port));
    fflush(stdout);


    int loc = 0;
    while(1) // Better signal handling required
    {
        if((new_socket = accept(sock, (struct sockaddr *)&client, (socklen_t*)&sockaddr_len)) == ERROR)
        {
            perror("accept");
            exit(-1);
        }

        //get the thread structure whose thread has already exited
        loc=look4FreeThreadID();
        thread_table[loc]=1;//mark the threadID as in-use
        //Initialize the thread argument
        struct threadParams arg;
        arg.instance = NULL;
        arg.threadID = loc;
        arg.socketID = new_socket;
        //Initialize the attributes
        pthread_attr_init(&thpool_attr[loc]);
        pthread_create(&thpool[loc], &thpool_attr[loc], control_thread, &arg);

        //Detach the thread so that we can use it again once it has exited
        pthread_detach(thpool[loc]);

        printf("New Client connected from port no %d and IP %s\n", ntohs(client.sin_port), inet_ntoa(client.sin_addr));
    }
    close(sock);
    return 0;
}
