/*
 * Cloud.h
 *
 *  Created on: 2013-10-11
 *      Author: mukesh
 */
#include<cv.h> /* required to use OpenCV */
#include<highgui.h> /* required to use OpenCV's highgui */
#include<stdio.h>
#include <sys/time.h>
#include "sys/socket.h"
#include "sys/types.h"
#include "netinet/in.h"
#include "error.h"
#include "strings.h"
#include "unistd.h"
#include "arpa/inet.h"
#include <pthread.h>
#include <semaphore.h>

#include"Client.h"
//#include"Capture.h"
//#include"Frame.h"

using namespace std;
using namespace cv;

#define BUFFER    1024

// A Simple Camera Capture Framework
int main(int argc, char **argv)
{

	//===============Parse the command line arguments here=======================
	if(argc!=3){
		fprintf( stderr, "Usage: ./client server_IP_addr server_port_number\n");
		return -1;
	}

    struct sockaddr_in serv;
    int sock;
    char in[BUFFER];
    char out[BUFFER];


//===SOCKET CALLS===//

    //Open socket
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket");
        exit(-1);
    }

    //Initialize socket attributes for server connection.
    serv.sin_family = AF_INET;
    serv.sin_port = htons(atoi(argv[2]));
    serv.sin_addr.s_addr = inet_addr(argv[1]);
    bzero(&serv.sin_zero, 8);


    printf("The TCPclient %d\n",ntohs(serv.sin_port));
        fflush(stdout);



    //Send request to the server
    if((connect(sock, (struct sockaddr *)&serv, sizeof(struct sockaddr_in))) == -1)
    {
        perror("connect");
        exit(-1);
    }

//===SEND CAMERA ADDRESSES TO THE SERVER===//
    printf("\nPlease provide camera addresses separated by space: ");
    fgets(in, BUFFER, stdin);
    send(sock, in, strlen(in), 0);


    int len=1;
    while(len)
    {
        len = recv(sock, out, BUFFER, 0);
        out[len] = '\0';

        printf("Server reply: %s\n", out);

    	printf("\nInput: ");

        fgets(in, BUFFER, stdin);
        send(sock, in, strlen(in), 0);



    }

    close(sock);

    return 0;

}
