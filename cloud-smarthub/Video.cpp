/*
 * Video.cpp
 *
 *  Created on: 2013-10-26
 *      Author: mukesh
 */
#include<cv.h> /* required to use OpenCV */
#include<highgui.h> /* required to use OpenCV's highgui */
#include<stdio.h>
#include <sys/time.h>
#include "sys/socket.h"
#include "sys/types.h"
#include "netinet/in.h"
#include "error.h"
#include "strings.h"
#include "unistd.h"
#include "arpa/inet.h"



#include "AdaptiveMedianBGS.hpp"
#include "GrimsonGMM.hpp"
#include "ZivkovicAGMM.hpp"
#include "MeanBGS.hpp"
#include "WrenGA.hpp"
#include "PratiMediodBGS.hpp"
#include "Eigenbackground.hpp"

#include "Video.h"

 using namespace std;
 using namespace cv;


Video::Video(){

}

double Video::DiffClock(clock_t clock1,clock_t clock2)
 {
 	double diffticks=clock1-clock2;
 	double diffms=(diffticks*1000)/CLOCKS_PER_SEC;
 	return diffms;
 }

Video::Video(CvCapture* capture, int cam_num, int speed){

	camera_number = cam_num;

	//Blob Detection Initializations
	width	= (int) cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH);
	height = (int) cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT);
	params.SetFrameSize(width, height);
	params.LowThreshold() = 3.0f*3.0f;
	params.HighThreshold() = 2*params.LowThreshold();	// Note: high threshold is used by post-processing
	params.Alpha() = 0.001f*speed;
	params.MaxModes() = 3;
	frame_index=0;
	bgs.Initalize(params);
	low_threshold_mask = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	low_threshold_mask.Ptr()->origin = IPL_ORIGIN_BL;
	high_threshold_mask = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	high_threshold_mask.Ptr()->origin = IPL_ORIGIN_BL;
	frame_data = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 3);
	storage = cvCreateMemStorage(0);
	//Allocate memory for blob data
	Blobs.resize(MAX_NUM_BLOBS);
	blob_available.resize(MAX_NUM_BLOBS);

	//Face detection Initializations
	 face_cascade_name = "haarcascade_frontalface_alt.xml";
	 eyes_cascade_name = "haarcascade_eye_tree_eyeglasses.xml";
	  //-- 1. Load the cascades
	  if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading\n");  };
	  if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading\n"); };

	  stringstream ss;
	  ss<<"V"<<camera_number<<".txt";
	  String filename=string(ss.str());
	  results.open(filename.c_str());
	  //Initialize to write results for the zeroth file
	  results<<"Frame tBgFg tUpdate tDetect   Blobs   Faces   tFace"<<endl<<frame_index;

}
Video::~Video(){
	//Equivalent to CvReleaseImage
	low_threshold_mask.ReleaseMemory(1);
	high_threshold_mask.ReleaseMemory(1);
	frame_data.ReleaseMemory(1);
	cvClearMemStorage(storage);
	results.close();
}

int Video::WriteResults(){
	results.close();
	return 1;
}

int Video::IncreaseFrameIndex(){
	frame_index++;

	//add a new line to the result file and the next frame_index
	results<<endl;
	results<<frame_index;

	return frame_index;
}

int Video::FindFaces(IplImage* frame){

	  Mat frame_temp=frame;
	  RNG rng(12345);
	  std::vector<Rect>faces_new;
	  Mat frame_gray;

	  //===
	  begin_clock = clock();

	  cvtColor( frame_temp, frame_gray, CV_BGR2GRAY );
	  equalizeHist( frame_gray, frame_gray );

	  //-- Detect faces
	  face_cascade.detectMultiScale( frame_gray, faces_new, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );

	  face_count=faces_new.size();

/*	  for( int i = 0; i < faces.size(); i++ )
	  {
	    Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
	    ellipse( frame, center, Size( faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );

	    Mat faceROI = frame_gray( faces[i] );
	    std::vector<Rect> eyes;

	    //-- In each face, detect eyes
	    eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(30, 30) );

	    for( int j = 0; j < eyes.size(); j++ )
	     {
	       Point center( faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5 );
	       int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
	       circle( frame, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
	     }
	  }
*/
	  results<<"\t"<<face_count;

	  end_clock = clock();

	  results<<"\t"<<double(DiffClock(end_clock, begin_clock));
	  return face_count;
}

int Video::FindBlobs(IplImage* frame){

	IplConvKernel *StructuringElement1 = cvCreateStructuringElementEx(3, 3,2, 2, CV_SHAPE_ELLIPSE, NULL);
	IplConvKernel *StructuringElement2 = cvCreateStructuringElementEx(15, 15,8, 8, CV_SHAPE_ELLIPSE, NULL);

	//Just a wrapper over IplImage to provide some easy function to access pixel values etc.
	frame_data = frame;
	//If it is the first frame, initialize the
	if(frame_index==0){
		bgs.InitModel(frame_data);
	}

	begin_clock = clock();
	// perform background subtraction
	bgs.Subtract(frame_index, frame_data, low_threshold_mask, high_threshold_mask);
	end_clock = clock();
	results<<"\t"<<double(DiffClock(end_clock, begin_clock));


	//Perform erosion or dilation if required
	for (int k = 0; k <1; k++)
    {
		cvErode ( high_threshold_mask.Ptr(), high_threshold_mask.Ptr(), StructuringElement1, 2);
		cvDilate( high_threshold_mask.Ptr(),high_threshold_mask.Ptr(), StructuringElement2, 2);

	}

	begin_clock = clock();
	//Detect number of blobs
	blob_count = MaxAreaContour(frame_data.Ptr(), high_threshold_mask.Ptr());
	end_clock = clock();
	results<<"\t"<<double(DiffClock(end_clock, begin_clock));

    results<<"\t"<<blob_count;
	// update background subtraction
	low_threshold_mask.Clear();	// disable conditional updating

	begin_clock = clock();
	bgs.Update(frame_index, frame_data, low_threshold_mask);
	end_clock = clock();
	results<<"\t"<<double(DiffClock(end_clock, begin_clock));
}

int Video::MaxAreaContour(IplImage *currImg, IplImage *BinImage)
{
	//int max_num_contours = 15;
	CvRect LocalRect[MAX_CONTOURS];
	int center_x0;
	int center_x1;

	//mergeFlag[0] = 0;

	bool white[MAX_CONTOURS];//used in merging phase
	//My variables
	//int mk_AreaArray[MAX_NUM_BLOBS];//For temporarily soring the arrays.
	int i,j,num_of_contours;

	CvSeq* contour=0;
	//CvSeq* tempcontour[2];

	//tempcontour[0] =0; tempcontour[1] = 0;
	if (contour != NULL)
	{
		contour->h_next = 0;
	}


	//Find the all the contours in the binary image.
	cvFindContours( BinImage, storage, &contour, sizeof(CvContour), CV_RETR_LIST,CV_CHAIN_APPROX_SIMPLE );
    //====================================================//
    //////////////Read the Blobs in one array////////////
	num_of_contours=-1;
	for( ; contour != NULL; contour = contour->h_next)
	{
       if (fabs(cvContourArea( contour, CV_WHOLE_SEQ ))> int(MIN_CONTOUR_AREA*width))//Only consider big enough contours
	   {
		   if (num_of_contours == (MAX_CONTOURS-1))//We only read MAX_NUM_BLOBS contours
				break;
			else
				num_of_contours++;


			//rect = cvBoundingRect(contour, 0 );
		   LocalRect[num_of_contours] = cvBoundingRect(contour, 0 );
		   white[num_of_contours] = true;
	   }
	}

    num_of_contours++;//The number of contours is 0,1,...num_of_countours-1, it also shows number of blobs
	//======================================================//
	/////////Sort the blobs///////////////////////////////
	CvRect temp_rect;
	for (i=0; i< num_of_contours ; i++)
		for (j=i+1; j<num_of_contours;j++)
		{
			if((LocalRect[j].width*LocalRect[j].height)>(LocalRect[i].width*LocalRect[i].height))//See if there is any bigger blob
			{
				temp_rect.x		= LocalRect[i].x;
				temp_rect.y		= LocalRect[i].y;
				temp_rect.width	= LocalRect[i].width;
				temp_rect.height = LocalRect[i].height;

				LocalRect[i].x		= LocalRect[j].x;
				LocalRect[i].y		= LocalRect[j].y;
				LocalRect[i].width	= LocalRect[j].width;
				LocalRect[i].height = LocalRect[j].height;

				LocalRect[j].x		= temp_rect.x;
				LocalRect[j].y		= temp_rect.y;
				LocalRect[j].width	= temp_rect.width;
				LocalRect[j].height = temp_rect.height;

			}
		}
		//=====================================================//
    //////Run the merge algorithm////////////////////////////



	for (i=0; i <num_of_contours-1;i++)//till second last blob
	{
		if(white[i])//Only consider the blobs which have not been merged so far
			for (j=i+1; j<num_of_contours;j++)//till last blob, only blobs which are available
			{
				if(white[j])
				{


					center_x0 = (int) (LocalRect[i].x + (float)(LocalRect[i].width/2.0));
					center_x1 = (int) (LocalRect[j].x + (float)(LocalRect[j].width/2.0));
					///checking if the smaller rectangle (LocalRect[j]) is not inside larger rectangel (LocalRect[i])
					if((LocalRect[i].x <= LocalRect[j].x) && (LocalRect[i].y <= LocalRect[j].y) &&
						(LocalRect[i].x+LocalRect[i].width >= LocalRect[j].x+LocalRect[j].width) &&
						(LocalRect[i].y+LocalRect[i].height >= LocalRect[j].y+LocalRect[j].height))
					{

 							white[j] = false;//The blob is merged with blob i

					}
					else if ( abs(center_x0 - center_x1) < int(MIN_MERGE_DISTANCE*width))
					{
						white[j] = false;//The blob is merged to the bigger blob

						if (LocalRect[i].y < LocalRect[j].y)	// LocalRect[j] is below than LocalRect[i]
						{

							//Merge and update LocalRect[i]
							//LocalRect[i].x		= LocalRect[i].x;
							//LocalRect[i].y		= LocalRect[i].y;
							//LocalRect[i].width	= LocalRect[i].width;
							//LocalRect[i].height = LocalRect[j].y - LocalRect[i].y + LocalRect[j].height;
							if(LocalRect[j].x<LocalRect[i].x) // j is left to i
							{
								LocalRect[i].width = max(LocalRect[i].x - LocalRect[j].x + LocalRect[i].width,LocalRect[j].width);
								LocalRect[i].x = LocalRect[j].x;
							}
							else
							{
								LocalRect[i].width = max(LocalRect[j].x - LocalRect[i].x + LocalRect[j].width,LocalRect[i].width);
							}


							LocalRect[i].height = max(LocalRect[j].y - LocalRect[i].y + LocalRect[j].height,LocalRect[i].height);

						}
						else					// LocalRect[i] is below than LocalRect[j]
						{

							//LocalRect[i].x		= LocalRect[i].x;
							//LocalRect[i].y		= LocalRect[j].y;
							//LocalRect[i].width	= LocalRect[i].width;
							//LocalRect[i].height = LocalRect[i].y - LocalRect[j].y + LocalRect[i].height;

							if(LocalRect[j].x<LocalRect[i].x) // j is left to i
							{

								LocalRect[i].width = max(LocalRect[i].x - LocalRect[j].x + LocalRect[i].width,LocalRect[j].width);
								LocalRect[i].x = LocalRect[j].x;
							}
							else
							{
								LocalRect[i].width = max(LocalRect[j].x - LocalRect[i].x + LocalRect[j].width,LocalRect[i].width);
							}

							LocalRect[i].height = max(LocalRect[i].y - LocalRect[j].y + LocalRect[i].height,LocalRect[j].height);
							LocalRect[i].y		= LocalRect[j].y;
						}
					}

				}
			}


	}

	//====================================================//
	////////////Re-collect the resulting blobs//////////////
	i=0; //First blob (i=0) can never be merged, so start from second blob
	for (j=0; j< num_of_contours;j++)
		if(white[j])//The blob has not been merged with other blobs
		{
			if(j < MAX_NUM_BLOBS)
			if((LocalRect[j].width*LocalRect[j].height)>int(MIN_BLOB_AREA*width))//check if the blob is of sufficient size
			{
				Blobs[i].x		= LocalRect[j].x;
				Blobs[i].y		= LocalRect[j].y;
				Blobs[i].width	= LocalRect[j].width;
				Blobs[i].height = LocalRect[j].height;
				blob_available[i]=true;
				i=i+1;
			}
		}

	int num_of_blobs = i;
	//===================================================//

	    //Draw the blobs in the output file=////////////////////
		int scale = 1;
		//for(i=0;i<num_of_blobs;i++)
		for(i=0;i<num_of_blobs;i++)
		{
			cvRectangle( currImg, cvPoint(Blobs[i].x*scale,Blobs[i].y*scale),
			  cvPoint((Blobs[i].x+Blobs[i].width)*scale,
					 (Blobs[i].y+Blobs[i].height)*scale),
			  CV_RGB(255,0,0), 2 );
		      //if(num_of_blobs==2)
				//       num_of_blobs = 2;
		}


		//====================================================//



	//cvReleaseImage(&BinImage);

	//cvClearMemStorage(contour->storage);
	//cvReleaseMemStorage(&contour->storage);
	cvClearMemStorage(storage);
	//cvReleaseMemStorage(&storage);
    blob_count = num_of_blobs;
	return num_of_blobs;
}


