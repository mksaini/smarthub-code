/*
 * Cloud.cpp
 *
 *  Created on: 2013-10-11
 *      Author: mukesh
 */
#include<cv.h> /* required to use OpenCV */
#include<highgui.h> /* required to use OpenCV's highgui */
#include<stdio.h>
#include <sys/time.h>
#include "sys/socket.h"
#include "sys/types.h"
#include "netinet/in.h"
#include "error.h"
#include "strings.h"
#include "unistd.h"
#include "arpa/inet.h"
#include "Video.h"

//#include"Cloud.h"
#include"Smarthub.h"

SmartHub::SmartHub(){

}

SmartHub::SmartHub(int num_of_cameras){

	//Initialize the camera captures
	capture.resize(num_of_cameras);

	//Initialize the IplImage
	frame.resize(num_of_cameras);

	//Initialize video objects
    video.resize(num_of_cameras);

	//Indicate that the SmartHub is alive
	alive = 1;

	//Process flab
	process = false;

	//Total number of cameras should be known to all functions
	NUM_OF_CAMERAS = num_of_cameras;
}

SmartHub::~SmartHub(){

	//Whatever you allocate in the constructor, or somewhere else, de-allocate in the destructor
	for(int cam_num=0; cam_num<NUM_OF_CAMERAS; cam_num++){

		//Open the camera captures
		cvReleaseCapture(&capture[cam_num]);
		//Initialize video objects, for now we keep the speed to be 1 (little slow)
		delete video[cam_num];
	}

}

int SmartHub::InitializeVars(vector<string> camAddrVector){

	for(int cam_num=0; cam_num<NUM_OF_CAMERAS; cam_num++){

		//Open the camera captures
		capture[cam_num] = cvCaptureFromFile(camAddrVector[cam_num].c_str());
		if (!capture[cam_num]) {
			fprintf(stderr, "Cannot open capture\n");
			alive= 0;
			//pthread_exit(NULL);
		}

		//Initialize video objects, for now we keep the speed to be 1 (little slow)
		video[cam_num] = new Video(capture[cam_num], cam_num, 1);
	}

}

void SmartHub::MainThread(){

	//Open a window for testing
	//cvNamedWindow( "mywindow", CV_WINDOW_AUTOSIZE);

	while(alive){


		//Repeat for all cameras
		for(int cam_num=0; cam_num<NUM_OF_CAMERAS; cam_num++){

			//grab frame
			frame[cam_num] = cvQueryFrame(capture[cam_num]);


			video[cam_num]->FindBlobs(frame[cam_num]);

			video[cam_num]->FindFaces(frame[cam_num]);

			//Processing part
			int current_frame_index = video[cam_num]->IncreaseFrameIndex();

			//Stop at some condition
			if(current_frame_index == 2000)
				alive=false;
			//display frame
//	   	    if (!frame[cam_num]) {
//	   	        printf("Unable to read input\n");
//				alive= 0;
//				//pthread_exit(NULL);
//	   	    }
			cout<<"In while loop. Blobs = "<<video[cam_num]->blob_count<<" Faces = "<<video[cam_num]->face_count<<"\n";
	   	    ///sleep(1);
//	   	    cvShowImage( "mywindow",frame[cam_num]);
	   	    //cvWaitKey(0);

			//Increment the frame in

		}

	}
	for(int cam_num=0; cam_num<NUM_OF_CAMERAS; cam_num++){
		video[cam_num]->WriteResults();
	}
	//cvDestroyWindow( "mywindow" );
}

void SmartHub::KillMainThread(){
	alive=false;
}

