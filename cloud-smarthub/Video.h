/*
 * Video.h
 *
 *  Created on: 2013-10-26
 *      Author: mukesh
 */

#ifndef VIDEO_H_
#define VIDEO_H_

#include "AdaptiveMedianBGS.hpp"
#include "GrimsonGMM.hpp"
#include "ZivkovicAGMM.hpp"
#include "MeanBGS.hpp"
#include "WrenGA.hpp"
#include "PratiMediodBGS.hpp"
#include "Eigenbackground.hpp"
#include <fstream>

//Blob detection related macros
#define MIN_MERGE_DISTANCE 0.01//low value increases number of blobs, good for counting number of people.
#define MAX_NUM_BLOBS 5  //Should not be more than MAX_CONTOURS
#define MIN_BLOB_AREA 0.8
#define MAX_CONTOURS 200
#define MIN_CONTOUR_AREA 0.8



using namespace std;
using namespace cv;


class Video {

private:
	//For blob detection
	Algorithms::BackgroundSubtraction::GrimsonGMM bgs; // Can choose which algorithm to use here
	Algorithms::BackgroundSubtraction::GrimsonParams params;//Parameter initialization. Exact parameters would depend on the algorithm.
	// setup marks to hold results of low and high thresholding - these hold results
	BwImage low_threshold_mask;
	BwImage high_threshold_mask;
	RgbImage frame_data;
	//Temporary storage to store the linked list of blobs.
	CvMemStorage* storage;//Its one block of memory which can be used by any function
	int			MergeFlag;

	//For face detection


public:

	//=== Temporary to record timings
	ofstream results;
	clock_t begin_clock;
	clock_t end_clock;
	double DiffClock(clock_t clock1,clock_t clock2);
	 //===
	//generic video parameters
	int camera_number;
	int width;
	int height;

	unsigned long frame_index;

	//===Blob related variables
	int blob_count;
	vector<CvRect> Blobs;
	vector<bool> blob_available;

	//Face related variables
	int face_count;
	vector<CvRect> faces_old;

	//===Face related parameters
	 String face_cascade_name;
	 String eyes_cascade_name;
	 CascadeClassifier face_cascade;
	 CascadeClassifier eyes_cascade;

public:
	//constructor1
	Video();

	//constructor2:
	Video(CvCapture* capture, int cam_num, int speed);

	//destructor
	~Video();

    //Detect blobs
	int FindBlobs(IplImage* frame);

	//Supporting function for blob detection
	int MaxAreaContour(IplImage *currImg, IplImage *BinImage);

	//Detect Faces
	int FindFaces(IplImage* fram);

	//for specifically closing the result files
	int WriteResults();

	//Increment the index to the next frame
	int IncreaseFrameIndex();
};


#endif /* VIDEO_H_ */
